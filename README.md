framaquette-thermo
==================

* La maquette de base LaTeX pour Framabook
* Modifications minimales pour les besoins du livre de thermodynamique


Remerciements
=============

Merci à :
* Vincent Lozano, 
* Didier Roche, 
* David Dauvergne, 
* Christophe Masutti, 
* Olivier Cleynen


Notes
=============

La maquette utilise aussi les styles suivants :

* picins.sty : voir http://www.ctan.org/pkg/picins

* fncychapleo.sty : http://tel.archives-ouvertes.fr/docs/00/48/59/33/TEX/fncychapleo.sty

